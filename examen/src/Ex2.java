
public class Ex2 {
    public static void main(String[] args) throws InterruptedException{
        Thread.currentThread().setName("RThread1");//firul main devine RThread 1 si vom mai crea un fir
        Thread t2= new Thread() {
            @Override
            public void run() {
                try {
                    writeMessages(12);
                } catch (InterruptedException ignored) {
                }
            }
        };
        t2.setName("RThread2");
        t2.start();
        writeMessages(12);
    }
    public static void writeMessages(int n) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("[%s] - [%d]", Thread.currentThread().getName(), i));
            Thread.sleep(3000);
        }
    }
}
