public class Ex1 {
    public static void main(String[] args) {
        B b=new B(123);
        G g=new G();
        E e=new E(b,g);
    }
}

interface C {
}

class B implements C {
    private long t;

    B(long t) {
        this.t = t;
    }

    public void x() {
    }
}

class A {
    public void met(B b) {
    }
}

class D {
    private B b;

    D(B b) {
        this.b = b;
    }
}

class F {
    public void metA() {
    }
}

class G {
}

class E {
    private B b;
    private F f;
    private G g;

    E(B b, G g) {
        this.b = b;
        this.g = g;
        this.f = new F();
    }

    public void metG(int i) {
    }
}
